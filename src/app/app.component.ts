export class AppComponent {
  revistas: Stage[] = [];
  cites: Cite[] = [];
  articulos = [];
  clicked: boolean = false;

  constructor(private service: DataService) {}

  ngOnInit() {
    this.setJournals();
  }

  private setJournals(): void {
    this.service.getData().subscribe((res) => {
      this.revistas = res.stages;
    });
  }

  onRevistaClicked(articulos, cites) {
    this.articulos = articulos;
    this.cites = cites;
    this.clicked = true;
  }
}
