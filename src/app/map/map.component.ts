var object : any;

export class MapComponent {

  constructor(private coordService: CoordinatesService) {}

  chargeMap(): void {
    // this.initMap();
    mapboxgl.accessToken =
      'Aqui va el ApiKey';
    const map = new mapboxgl.Map({
      container: 'map',
      style: 'Style',
      center: [-75.58239355524906, 6.222023784498981],
      zoom: -10,
    });

    map.on('load', function () {
      map.addSource('trees', {
        type: 'geojson',
        data: object,
      });
      // add heatmap layer here
      map.addLayer(
        {
          id: 'trees-heat',
          type: 'heatmap',
          source: 'trees',
          maxzoom: 15,
          paint: {
            // increase weight as diameter breast height increases
            'heatmap-weight': {
              property: 'dbh',
              type: 'exponential',
              stops: [
                [1, 0],
                [62, 1],
              ],
            },
            // increase intensity as zoom level increases
            'heatmap-intensity': {
              stops: [
                [11, 1],
                [15, 3],
              ],
            },
            // assign color values be applied to points depending on their density
            'heatmap-color': [
              'interpolate',
              ['linear'],
              ['heatmap-density'],
              0,
              'rgba(236,222,239,0)',
              0.2,
              'rgb(208,209,230)',
              0.4,
              'rgb(166,189,219)',
              0.6,
              'rgb(103,169,207)',
              0.8,
              'rgb(28,144,153)',
            ],
            // increase radius as zoom increases
            'heatmap-radius': {
              stops: [
                [11, 15],
                [15, 20],
              ],
            },
            // decrease opacity to transition into the circle layer
            'heatmap-opacity': {
              default: 1,
              stops: [
                [14, 1],
                [15, 0],
              ],
            },
          },
        },
        'waterway-label'
      );

      // add circle layer here

      map.addLayer(
        {
          id: 'trees-point',
          type: 'circle',
          source: 'trees',
          minzoom: 14,
          paint: {
            // increase the radius of the circle as the zoom level and dbh value increases
            'circle-radius': {
              property: 'dbh',
              type: 'exponential',
              stops: [
                [{ zoom: 15, value: 1 }, 5],
                [{ zoom: 15, value: 62 }, 10],
                [{ zoom: 22, value: 1 }, 20],
                [{ zoom: 22, value: 62 }, 50],
              ],
            },
            'circle-color': {
              property: 'dbh',
              type: 'exponential',
              stops: [
                [0, 'rgba(236,222,239,0)'],
                [10, 'rgb(236,222,239)'],
                [20, 'rgb(208,209,230)'],
                [30, 'rgb(166,189,219)'],
                [40, 'rgb(103,169,207)'],
                [50, 'rgb(28,144,153)'],
                [60, 'rgb(1,108,89)'],
              ],
            },
            'circle-stroke-color': 'white',
            'circle-stroke-width': 1,
            'circle-opacity': {
              stops: [
                [14, 0],
                [15, 1],
              ],
            },
          },
        },
        'waterway-label'
      );
    });
  }

  //si el service coordinates retorna true entonces se carga el nuevo objeto con los dos puntos base (o variable object) que tiene la clase comparadora, si retorna false, se carga con el objeto que hay en esta clase con dos posiciones base
  async update() {
    const allowed = await this.coordService.coordinates();
    if(allowed){
      console.log('Objeto desde la clase mapa antes de cargar|')
      console.log(object)
      object = await this.coordService.getCoordinates();
      console.log('Objeto desde la clase mapa cargado|')
      console.log(object)
      this.chargeMap();
    }else{
      console.log('Message from map.component: Cargo object base en mi clase')
      this.chargeMap()
    }
  }
}
