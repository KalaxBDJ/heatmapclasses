export class JournalComponent implements OnInit {
  @Input('journals') revistas: Stage[] = [];
  @Output() change = new EventEmitter<any>();
  cites = [];

  constructor(private service: DataService) {}


  revistaClicked(i: number) {
    this.cites = [];
    this.service.getCitesByJournal().subscribe((res) => {
      this.cites = res.cites[i].articulos;

      this.change.emit({
        articulos: this.revistas[i].articulos,
        citas: this.cites,
      });
    });
  }
}
