export interface RespuestaCites {
  name: string;
  cites: Cite[];
}

export interface Cite {
  name: string;
  articulos: ArticuloCite[];
}

export interface ArticuloCite {
  _id: Id;
  PDF: string;
  abstract: string;
  profiles: string;
  Journal: string;
  publisher: string;
  authors: string;
  title: string;
  pid: number;
  ref: string;
  Year?: number | number;
  cites: number;
  countries: string;
  affiliations: string;
  type: string;
  number?: any;
  volume?: any;
  cites_link?: string;
}

export interface Id {
  $oid: string;
}

export interface RespuestaStages {
  stages: Stage[];
}

export interface Stage {
  name: string;
  articulos: ArticuloStage[];
}

export interface ArticuloStage {
  _id: number;
  authors: string;
  cites: number;
  cites_link?: string;
  publisher?: string | string;
  abstract?: string | string;
  profiles: Profile | string | string;
  title: string;
  Journal: string;
  PDF: string;
  Year: number;
  bibtex: string;
  DOI?: any;
  countries?: string;
  type?: string;
  affiliations?: string;
  volume?: any;
  number?: any;
}

export interface Profile {}

export interface geoJson {
  type?: string;
  features?: Feature[];
}

export interface Feature {
  type: string;
  properties: Properties;
  geometry: Geometry;
}

export interface Geometry {
  type: string;
  coordinates: number[];
}

interface Properties {
  dbh: number;
}

export interface map {
  type: string;
  features: [{
    type: string;
    properties: {
      dbh: number;
    };
    geometry: {
      type: string;
      coordinates: number[];
    };
  }];
}
