export class CoordinatesService {
  map: any = null;

  constructor() {}

  async setCoords(object: any) {
    this.map = await object;
  }

  //si existe un objeto map, entonces retorna true para que la clase mapa pueda acceder al objeto en el sigueinte método; sino se carga la base que está en la clase mapa
  coordinates() {
    return new Promise((resolve) => {
      if (this.map != null) {
        resolve(true);
      } else {
        console.log('No existe mapa o no se ha cargado correctamente');
        resolve(false);
      }
    });
  }

  //método al cual va a acceder la clase mapa si se retornó true en el anterior
  getCoordinates() {
    return this.map;
  }
}
