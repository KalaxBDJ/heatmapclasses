export class DataService {

  private stagesUrl = 'JsonRevistas';
  private citesUrl = 'JsonCitas';
  private cites : Cite[] = [];

  constructor(private http : HttpClient) {  

    
   }

  getData()
  {
    return this.http.get<RespuestaStages>(this.stagesUrl);
  }

  getCitesByJournal()
  {
    return this.http.get<RespuestaCites>(this.citesUrl);
  }

  
}
