var object = {
  "type":"FeatureCollection","features":[
    {"type":"Feature","properties":{"dbh":200},"geometry":{"type":"Point","coordinates":[-75.57046308955081,6.212744598839561]}},
    {"type":"Feature","properties":{"dbh":200},"geometry":{"type":"Point","coordinates":[-75.5998171850098,6.237830019933771]}}
  ]
}

export class ComparadoraComponent implements OnInit , OnChanges {

  config = {
    flagDir: ''
  };

  obj = new framework(this.config);

  @Input() articulos : ArticuloStage[] = []
  @Input() citas : ArticuloCite[] = [];
  loading : boolean ;



  paises = []

  latlongs = []

  map: map = {type:'FeatureCollection', features:[{type:'Feature',properties:{dbh:0},geometry:{type:'Point',coordinates:[-79.91746,40.44356]}}]};

  constructor(private coordService: CoordinatesService) { }

  ngOnInit(): void {

   
  }

  onEventUpdate(){
      
      this.comparar();
      this.convertir();
    
  }
  


  comparar()
  {
    this.paises = []
    
    for(let i = 0 ; i < this.articulos.length ; i++)
    {
      for(let j =0 ; j < this.citas.length ; j++)
      {
        if(this.articulos[i]._id === this.citas[j].pid)
        {
          if(this.paises.find(pais => pais === this.citas[j].countries))
          {
            //ya existe
          }
          else
          {
            let substradted = this.citas[j].countries.substr(-20,2)

            if(this.paises.find(pais => pais === substradted))
            {
              //ya existe
            }
            else if(substradted.length === 0 )
            {
              //vacio
            }
            else
            {
              this.paises.push(substradted)
            }
          }
         
        }
      }
    }

    console.log(this.paises)

  }

  //el object está construído con dos coordenadas base para que se muestren en el map mientras se les quita el "lat" "long"
  convertir()
  {
    this.latlongs = []
    object.features = []

    for( let i = 0 ; i < this.paises.length ; i++ )
    {
      let latlongs = this.obj.latlong(this.paises[i])
      console.log('Aqui creo el objeto temporal: ',latlongs)
      object.features.push({"type":"Feature","properties":{"dbh":200},"geometry":{"type":"Point","coordinates":[latlongs.long,latlongs.lat]}})
    }
    console.log('Message from comparadora.component: envío dos coordenadas base para visualizar los cambios mientras se elimina las palabras lat, long del geometry. Click en actualizar')
    console.log(object)
    this.coordService.setCoords(object)
  }
}
