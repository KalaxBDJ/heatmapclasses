import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css','../../../css/min.css']
})
export class NavbarComponent implements OnInit {

  imgpath = '../../assets/heat.png'

  constructor() { }

  ngOnInit(): void {
  }

}
